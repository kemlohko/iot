# iot_praktikum
# =============

## Einführung

Im Praktikum des Moduls Internet der Dinge beschäftigen Sie sich mit einem einfachen IoTSystem 
zur dezentralen Steuerung eines Aktors über einen Smart Contractim EthereumTestnetzwerk Ropsten.

In dem System kommen ein paar typische IoT-Technologien zum Einsatz, die wir in den Vorlesungen behandeln.


Das WLAN wird durch einen Access-Point ausgestrahlt, der über das Hochschulnetzwerk mit dem Internet verbunden ist. 
Genutzt werden soll ein Smart Contract im EthereumTestnetzwerk Ropsten, auf den über ein einzurichtendes MetaMask-Konto zugegriffen wird. 
Der Smart Contract hält den Zustand eines einfachen Aktors (ein- oder ausgeschaltet), der durch eine LED signalisiert wird.

Mit dem WLAN verbunden ist ein MQTT-Broker (Raspberry Pi mit MQTT-Mosquitto-Server), der im Wesentlichen zur Protokollierung 
der Modulzustände dient, zum Teil aber auch für die direkte Kommunikation zwischen den Modulen eingesetzt wird.


Die implementierten Anwendungen des Systems werden über ein Android-Smartphone mit der App ViWiAN 1 IoT gesteuert. 
Das Smartphone kommuniziert via BLE (Bluetooth Low Energy) mit einem ESP32-DevKitC im sogenannten Mastermodul. 
Das zweite ESP32DevKitC des Mastermoduls dient als MQTT-Client. Es kann außerdem Transaktionen auf den Smart Contract ausführen, um den Aktor zu steuern. 
Beide ESP32-DevKitC sind über eine serielle Schnittstelle miteinander verbunden und kommunizieren über UART (Universal Asynchronous Receiver Transmitter).

Das Aktormodul besteht aus einem ESP32-DevKitC an dem eine LED angeschlossen ist. Die LED simuliert einen einfachen Aktor, indem sie anzeigt, ob der Aktor ein- oder ausgeschaltet ist. 
Das Aktormodul liest den Zustand des Aktors aus dem Smart Contract im Ropsten-Netzwerk, um den Aktor entsprechend anzusteuern. Darüber hinaus arbeitet das Aktormodul als MQTT-Client.


Das System komplettieren zwei Sensormodule. Jedes Sensormodul besteht aus zwei ESP32-DevKitC, die über eine serielle UART-Schnittstelle verbunden sind. 
Das eine ESP32DevKitC dient als MQTT-Client. Das zweite ESP32-DevKitC kommuniziert via BLE mit einem Nordic Thingy52 ™. 

Das Thingy52 ™ und ist eine Plattform (Hard- und Firmware) zum schnellen Entwickeln und Ausprobieren von Prototypen und Demonstrationen. 
Es stellt eine Reihe von Umweltsensoren (Licht, Temperatur, Luftdruck, Luftfeuchtigkeit und CO 2 ) und Bewegungssensoren (Beschleunigungssensor, 9-Achsen-Bewegungssensor) aber auch Aktoren bzw. 
Signalgeber (RGB-LED und Lautsprecher) zur Verfügung. Die Kommunikation erfolgt mit Bluetooth Low Energy 5. Darüber hinaus verfügt das Thingy52 ™ über eine NFC(Near Field Communication)-Antenne. 
Versorgt wird das Thingy52 ™ durch einen LithiumPolymer-Akku, der über USB aufgeladen werden kann.