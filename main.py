# iot_praktikum
# Created at 2021-11-10 16:34:24.382033

import streams

# Ethereum modules
from blockchain.ethereum import ethereum
from blockchain.ethereum import rpc

# MQTT module
from mqtt import mqtt
# WiFi drivers
from espressif.esp32net import esp32wifi as net_driver # for ESP-32
from wireless import wifi

# Mcu module
import mcu

# SSL module for https
import ssl

# Configuration file
import config


# The SSL context is needed to validate https certificates
SSL_CTX = ssl.create_ssl_context(
    cacert=config.CA_CERT,
    options=ssl.CERT_REQUIRED|ssl.SERVER_AUTH
)


# Pin mode
pinMode(D13, OUTPUT)

# MQTT client
client = None
# global variables
reseting = False
is_wifi_connected = False
is_ropsten_connected = False
is_led_contract_loaded = False
is_mqtt_server_connected = False


# This function connect the board to wifi.
def connect_to_wifi():
    global is_wifi_connected
    net_driver.auto_init()
    wifi.link(config.WIFI_SSID, wifi.WIFI_WPA2, config.WIFI_PASSWORD)
    is_wifi_connected = True


# Turns the LED on or off.
def set_led(value):
    if(value == 1):
        digitalWrite(D13, HIGH)
    elif(value == 0):
        digitalWrite(D13, LOW)
    else:
        print("Error: LED value must be either 1 or 0")


# Set the global variable "reseting" to true.
def reset():
    global reseting
    reseting = True

# Reset the board. 
# Note: The LED continues to light up even after the board has been reset!!! Maybe we need to turn off the LED ourselves before resetting???
def reset_board():
    if(reseting):
        print("reseting in 3 secondes ...")
        sleep(3000)
        print("bye!")
        mcu.reset()


# MQTT callbacks

# Check whether the reset command comes from the master module and whether the message macth.
def is_reset_command(data):
    if ('message' in data):
        message = data['message']
        return message.topic == 'iot/master' and message.payload == 'Master: Actor-1 reset.'
    return False


# standby after switching on.
def actor_is_ready():
    print("Actor-1 is ready!")
    client.publish("iot/Actor-1/status", "Actor-1 is ready.")
    
# Acknowledgment of the reset command.
def actor_is_reset():
    client.publish("iot/Actor-1/status", "Actor-1 is reset.")
    
# Actor is switched on
def actor_is_on():
    print("Actor-1 is on")
    client.publish("iot/Actor-1/status", "Actor-1 is on.")
    
# Actor is switched off.
def actor_is_off():
    print("Actor-1 is off")
    client.publish("iot/Actor-1/status", "Actor-1 is off.")


# This function set the MQTT client, publish and subscribe topic.
def mqtt_messages():
    global client
    global is_mqtt_server_connected
    # Set the mqtt id to "zerynth-mqtt".
    print("Connecting to MQTT-Broker...")
    client = mqtt.Client("zerynth-mqtt", True)
    
    # Try connect to mosquitto server.
    for retry in range(10):
        try:
            client.connect("192.168.178.33", 1883)
            is_mqtt_server_connected = True
            print("Aktor-1 connected to broker!")
            break
        except Exception as e:
            print("Connecting...")
            
    # Subscribe to reset command from master module.
    # The subscribe function takes a list of topics.
    # A topic is represented as a list with a channel (eg: iot/master) and a qos (Quality of Service).
    client.subscribe([["iot/master", 1]])
    # configure callback for "PUBLISH" message.
    # If is_reset_command evaluates to true, then the reset function is called,
    # which sets reseting to true.
    client.on(mqtt.PUBLISH, reset, is_reset_command)
    # Start the mqtt loop. 
    # This start a non blocking thread to handle incoming packets.
    client.loop()
    
    
# This function connects to ropsten and creates an instance of led_contract
def contract_instance(contract_address):
    global is_ropsten_connected
    global is_led_contract_loaded
    
    # Init the RPC node
    print("Connecting to Ropsten...")
    ropsten = rpc.RPC(config.RPC_URL, ssl_ctx=SSL_CTX)
    is_ropsten_connected = True
    print("Connected to Ropsten")
    
    # Create an instance of our led_contract
    print("Create an instance of led_contract...")
    contract = ethereum.Contract(ropsten, contract_address)
    is_led_contract_loaded = True
    print("led_contract instance successfully created!")
    
    return contract
    
# Main programm
try:
    streams.serial()
    
    # Connect to WiFi network.
    print("Connecting to wifi...")
    connect_to_wifi()
    print("Wifi connected!")
    
    # led_contract instance.
    led_contract = contract_instance(config.CONTRACT_ADDRESS)
    # Register to readLed function .
    print("Register to readLed function...")
    led_contract.register_function("retrieve")
    print("Successfully register to readLed function!")
    
    # MQTT messages.
    mqtt_messages()
    
    # Check if actor is ready.
    if(is_wifi_connected and is_ropsten_connected and is_led_contract_loaded and is_mqtt_server_connected):
        actor_is_ready()

except Exception as e:
    print(e)
    
while True:
    # Publish "Actor-1 is reset" and reset the board if reseting is true.
    # Note: This loop runs each 10 seconds. 
    # So when the reset command arrives from the master module, 
    # the board may not be reset immediately because the loop may still be asleep. 
    # We have to optimize that !!!
    if(reseting):
        actor_is_reset()
        reset_board()
            
    # Call readLed function.
    # rv (returned value) should be an uint8
    print("Reading LED value from led_contract...")
    led_value = led_contract.call("retrieve", rv = (256, int))
    print("Setting LED value...")
    set_led(led_value)
    print("Done!")
    # Sleep 10 seconds
    sleep(10000)